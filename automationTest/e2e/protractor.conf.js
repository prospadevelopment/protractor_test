let SpecReporter = require('jasmine-spec-reporter').SpecReporter;

exports.config = {
  //General Configuration
  suites: {
    regression: 'tests/*.js'
  },
  baseUrl: 'http://automationpractice.com/index.php',
  
  getPageTimeout: 2400000,
  useAllAngular2AppRoots: true,

  //Browser related configuration
  directconnect: true,

  capabilities: {
    'browserName': 'chrome'
  },

  //Jasmine Framework related configuration
  framework: 'jasmine2',
  jasmineNodeOpts: {
    isVerbose: false,
    showColors: true,
    defaultTimeoutInterval: 1200000,
    includeStackTrace: true,
    print: function() {}
  },

  //Preparation before tests(specs/suites) are executed
  onPrepare: function () {
    
      jasmine.getEnv().clearReporters();

      //Add reporter and configuration for better console output
      jasmine.getEnv().addReporter(new SpecReporter({
        spec: {
          displayStacktrace: true,
          displayFailed: true,
          displaySuccessful: true,
          displayDuration: true
        },
        summary: {
          displayStacktrace: false,
          displayDuration: true,
          displayFailed: false,
          displaySuccessful: false
        }
      }));
  }
};